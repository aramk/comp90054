#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# I used this to quickly run each command from commands.txt

# Question 1
python pacman.py -l tinyMaze -p SearchAgent -q
python pacman.py -l mediumMaze -p SearchAgent -q
python pacman.py -l bigMaze -z .5 -p SearchAgent -q

# Question 2
python pacman.py -l tinyMaze -p SearchAgent -a fn=bfs -q
python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs -q

# Question 3
python pacman.py -l mediumMaze -p SearchAgent -a fn=ucs -q
python pacman.py -l mediumDottedMaze -p StayEastSearchAgent -q
python pacman.py -l mediumScaryMaze -p StayWestSearchAgent -q
python pacman.py -l bigMaze -p SearchAgent -a fn=ucs -q

# Question 4
python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic -q

# Question 5
python pacman.py -l tinyCorners -p SearchAgent -a fn=bfs,prob=CornersProblem -q
python pacman.py -l mediumCorners -p SearchAgent -a fn=bfs,prob=CornersProblem -q

# Question 6
python pacman.py -l mediumCorners -p AStarCornersAgent -z 0.5 -q

# Question 7
python pacman.py -l testSearch -p AStarFoodSearchAgent -q
python pacman.py -l tinySearch -p SearchAgent -a fn=astar,prob=FoodSearchProblem,heuristic=foodHeuristic -q
python pacman.py -l tinySearch -p SearchAgent -a fn=ucs,prob=FoodSearchProblem -q

python pacman.py -l trickySearch -p AStarFoodSearchAgent -q

# Question 8
python pacman.py -l bigSearch -p ClosestDotSearchAgent -z .5 -q
python pacman.py -l tinyMaze -p SearchAgent -a fn=ucs,prob=AnyFoodSearchProblem -q

python pacman.py -l bigSearch -p ApproximateSearchAgent -z .5 -q
