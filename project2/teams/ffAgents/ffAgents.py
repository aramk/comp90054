# ffAgents.py
# -----------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html
# This code have been adapted by Nir Lipvetzky and Toby Davies at Unimelb to run FF planner

from captureAgents import CaptureAgent
from captureAgents import AgentFactory
import distanceCalculator
import random, time, util
from game import Directions
import keyboardAgents
import game
from util import nearestPoint
import os

#############
# FACTORIES #
#############

NUM_KEYBOARD_AGENTS = 0
class ffAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, isRed, first='offense', second='defense', rest='offense'):
    AgentFactory.__init__(self, isRed)
    self.agents = [first, second]
    self.rest = rest

  def getAgent(self, index):
    if len(self.agents) > 0:
      return self.choose(self.agents.pop(0), index)
    else:
      return self.choose(self.rest, index)

  def choose(self, agentStr, index):
    if agentStr == 'keys':
      global NUM_KEYBOARD_AGENTS
      NUM_KEYBOARD_AGENTS += 1
      if NUM_KEYBOARD_AGENTS == 1:
        return keyboardAgents.KeyboardAgent(index)
      elif NUM_KEYBOARD_AGENTS == 2:
        return keyboardAgents.KeyboardAgent2(index)
      else:
        raise Exception('Max of two keyboard agents supported')
    elif agentStr == 'offense':
      return OffensiveReflexAgent(index)
    elif agentStr == 'defense':
      return DefensiveReflexAgent(index)
    else:
      raise Exception("No staff agent identified by " + agentStr)

class AllOffenseAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, **args):
    AgentFactory.__init__(self, **args)

  def getAgent(self, index):
    return OffensiveReflexAgent(index)

class OffenseDefenseAgents(AgentFactory):
  "Returns one keyboard agent and offensive reflex agents"

  def __init__(self, **args):
    AgentFactory.__init__(self, **args)
    self.offense = False

  def getAgent(self, index):
    self.offense = not self.offense
    if self.offense:
      return OffensiveReflexAgent(index)
    else:
      return DefensiveReflexAgent(index)

##########
# Agents #
##########

class ReflexCaptureAgent(CaptureAgent):
  def __init__( self, index, timeForComputing = .1 ):
    CaptureAgent.__init__( self, index, timeForComputing)
    self.visibleAgents = []

  def createPDDLobjects(self):
    """
    Return a list of objects describing grid positions of the layout
    This list is used in the PDDL problem file 
    """    
    
    obs = self.getCurrentObservation()
    grid = obs.getWalls()
    locations = grid.asList(False);

    result = '';
    for (x,y) in locations:
      result += 'p_%d_%d '%(x,y)

    return result

  def createPDDLfluents(self):
    """
    Return a list of fluents describing the state space This list is
    used in the PDDL domain file
    """
    
    result = ''
    obs = self.getCurrentObservation()

    #Example: adds a fluent to describe our agent position
    """
    (myx,myy) = obs.getAgentPosition( self.index )
    if obs.getAgentState( self.index ).isPacman is True:
      result += '(AGENT_AT p_%d_%d) '%(myx,myy)
    else:
      result += '(AGENT_AT p_%d_%d) '%(myx,myy)
      """

    #
    # Model opponent team if visible and store them in an array
    #
    
    """
    self.visibleAgents = []
    other_team = self.getOpponents( obs )
    distances = obs.getAgentDistances()
    for i in other_team:
      
      if obs.getAgentPosition(i) is None: continue
      
      (x,y) = obs.getAgentPosition(i)
    
      if obs.getAgentState(i).isPacman is False:
        result += '(ENEMY_PHANTOM_AT p_%d_%d) '%(x,y)
      else:
        result += '(ENEMY_PACMAN_AT p_%d_%d) '%(x,y)
        # stores an array with visible agents in current state
        self.visibleAgents.append( i )

        """

    #
    # Model teammate agents if visible
    #

    """
    team = self.getTeam( obs )
    teamPos = []
    for i in team:
      if obs.getAgentPosition(i) is None: continue
      if i == self.index: continue
      (x,y) = obs.getAgentPosition(i)
      teamPos.append( (x,y) )
      
      if obs.getAgentState(i).isPacman is False:
        result += '(PHANTOM_AT p_%d_%d) '%(x,y)
      else:
        result += '(PACMAN_AT p_%d_%d) '%(x,y)
        """


    #
    # Food. 
    #    
    """
    food = self.getFood( obs ).asList(True)
    for (x,y) in food:
      result += '(CHEESE_AT p_%d_%d) '%(x,y)
      """       
      
    #
    # Capsule
    #
    """
    capsule = self.getCapsules( obs )
    for (x,y) in capsule:
      result += '(CHEER_AT p_%d_%d) '%(x,y)
      """

    #
    # Describe where the agent can move
    #
    """
    grid = obs.getWalls()
    for y in range(grid.height):
      for x in range(grid.width):
        if grid[x][y] is False:
          if grid[x+1][y] is False:
            result += '(CAN_GO p_%d_%d p_%d_%d) '%(x,y,x+1,y)
            ETC...
       """   
    return result

  def createPDDLgoal( self ):
    """
    Return a list of fluents describing the goal states
    This list is used in the PDDL problem file 
    """
    
    result = ''
    obs = self.getCurrentObservation()

    return result

  def generatePDDLproblem(self): 
	"""
        outputs a file called problem.pddl describing the initial and
        the goal state
        """

	f = open("problem%d.pddl"%self.index,"w");
	lines = list();
	lines.append("(define (problem pacman-problem)\n");
   	lines.append("   (:domain pacman-strips)\n");
   	lines.append("   (:objects \n");
	lines.append( self.createPDDLobjects() + "\n");
	lines.append(")\n");
	lines.append("   (:init \n");
	lines.append("   ;primero objetos \n");
	lines.append( self.createPDDLfluents() + "\n");
        lines.append(")\n");
        lines.append("   (:goal \n");          
        lines.append("	( and  \n");
        lines.append( self.createPDDLgoal() + "\n");
        lines.append("	)\n");
        lines.append("   )\n");
        lines.append(")\n");

	f.writelines(lines);
	f.close();	
        

  def runPlanner( self ):
    """
    runs the planner with the generated problem.pddl. The domain.pddl
    in our case is always the same, as we simply describe once all
    possible actions. The output is redirected into a text file.
    """
    os.system("./ff -o ./domain.pddl -f ./problem%d.pddl > solution%d.txt"%(self.index,self.index) );

  def parseSolution( self ):
    """
    Parse the solution file of FF, and selects the first action of the
    plan. Note that you could get  all the actions, not only the first
    one, and call the planner just  when an action in the plan becomes
    unapplicable or the some new  relevant information of the state of
    the game has changed.
    """

    f = open("solution%d.txt"%self.index,"r");
    lines = f.readlines();
    f.close();

    for line in lines:
      pos_exec = line.find("0: "); #First action in solution file
      if pos_exec != -1: 
        command = line[pos_exec:];
        command_splitted = command.split(' ')
       
        x = int(command_splitted[3].split('_')[1])
        y = int(command_splitted[3].split('_')[2])

        return (x,y)

      #
      # Empty Plan, Use STOP action, return current Position
      #
      if line.find("ff: goal can be simplified to TRUE. The empty plan solves it") != -1:
        return  self.getCurrentObservation().getAgentPosition( self.index )


  def chooseAction(self, gameState):
    """
    Generate the PDDL problem representing the state of the problem and the goals of the agent.
    Run the planner.
    Parse the solution.
    Send the first action of the plan.
    """
    actions = gameState.getLegalActions(self.index)

    bestAction = 'Stop'
    
    self.generatePDDLproblem()
    self.runPlanner()
    (newx,newy) = self.parseSolution()

    for a in actions:
      succ = self.getSuccessor(gameState, a)
      if succ.getAgentPosition( self.index ) == (newx, newy):
        bestAction = a
        break

    return bestAction
 
  def getSuccessor(self, gameState, action):
    """
    Finds the next successor which is a grid position (location tuple).
    """
    successor = gameState.generateSuccessor(self.index, action)
    pos = successor.getAgentState(self.index).getPosition()
    if pos != nearestPoint(pos):
      # Only half a grid position was covered
      return successor.generateSuccessor(self.index, action)
    else:
      return successor

  def evaluate(self, gameState, action):
    """
    Computes a linear combination of features and feature weights
    """
    features = self.getFeatures(gameState, action)
    weights = self.getWeights(gameState, action)
    return features * weights

  def getFeatures(self, gameState, action):
    """
    Returns a counter of features for the state
    """
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)
    return features

  def getWeights(self, gameState, action):
    """
    Normally, weights do not depend on the gamestate.  They can be either
    a counter or a dictionary.
    """
    return {'successorScore': 1.0}

class OffensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that seeks food. This is an agent
  we give you to get an idea of what an offensive agent might look like,
  but it is by no means the best or only way to build an offensive agent.
  """
  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)
    features['successorScore'] = self.getScore(successor)

    # Compute distance to the nearest food
    foodList = self.getFood(successor).asList()
    if len(foodList) > 0: # This should always be True,  but better safe than sorry
      myPos = successor.getAgentState(self.index).getPosition()
      minDistance = min([self.getMazeDistance(myPos, food) for food in foodList])
      features['distanceToFood'] = minDistance
      print action, features, successor
    return features

  def getWeights(self, gameState, action):
    return {'successorScore': 100, 'distanceToFood': -1}

class DefensiveReflexAgent(ReflexCaptureAgent):
  """
  A reflex agent that keeps its side Pacman-free. Again,
  this is to give you an idea of what a defensive agent
  could be like.  It is not the best or only way to make
  such an agent.
  """

  def getFeatures(self, gameState, action):
    features = util.Counter()
    successor = self.getSuccessor(gameState, action)

    myState = successor.getAgentState(self.index)
    myPos = myState.getPosition()

    # Computes whether we're on defense (1) or offense (0)
    features['onDefense'] = 1
    if myState.isPacman: features['onDefense'] = 0

    # Computes distance to invaders we can see
    enemies = [successor.getAgentState(i) for i in self.getOpponents(successor)]
    invaders = [a for a in enemies if a.isPacman and a.getPosition() != None]
    features['numInvaders'] = len(invaders)
    if len(invaders) > 0:
      dists = [self.getMazeDistance(myPos, a.getPosition()) for a in invaders]
      features['invaderDistance'] = min(dists)

    if action == Directions.STOP: features['stop'] = 1
    rev = Directions.REVERSE[gameState.getAgentState(self.index).configuration.direction]
    if action == rev: features['reverse'] = 1

    return features

  def getWeights(self, gameState, action):
    return {'numInvaders': -1000, 'onDefense': 100, 'invaderDistance': -10, 'stop': -100, 'reverse': -2}


